export const getDefaultHeaders = () => {
  const token = localStorage.getItem("TOKEN");

  return {
    "x-access-token": token
  };
};
