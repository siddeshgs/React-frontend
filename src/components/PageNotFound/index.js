import React from "react";
import PropTypes from "prop-types";

export default function PageNotFound(props) {
  return <div>Page Not Found</div>;
}

PageNotFound.defaultProps = {};
PageNotFound.propTypes = {};
