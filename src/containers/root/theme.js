import { createMuiTheme } from "@material-ui/core/styles";
import DINRoundProBlack from "../../assets/fonts/DINRoundPro/DINRoundPro-Black.woff2";
import DINRoundProMedi from "../../assets/fonts/DINRoundPro/DINRoundPro-Medi.woff2";
import DINRoundProLight from "../../assets/fonts/DINRoundPro/DINRoundPro-Light.woff2";
import DINRoundPro from "../../assets/fonts/DINRoundPro/DINRoundPro.woff2";
import DINRoundProBold from "../../assets/fonts/DINRoundPro/DINRoundPro-Bold.woff2";

const DINRoundPro_Theme = {
  fontFamily: "DINRoundPro",
  fontStyle: "normal",
  fontWeight: "normal",
  src: `
      local('DINRoundPro'),
      url(${DINRoundPro}) format('woff2')
    `,
  unicodeRange:
    "U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF"
};

const DINRoundProLight_Theme = {
  fontFamily: "DINRoundProLight",
  fontStyle: "normal",
  fontWeight: 300,
  src: `
        local('DINRoundProLight'),
        url(${DINRoundProLight}) format('woff2')
      `,
  unicodeRange:
    "U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF"
};

const DINRoundProMedi_Theme = {
  fontFamily: "DINRoundProMedi",
  fontStyle: "normal",
  fontWeight: 500,
  src: `
        local('DINRoundProMedi'),
        url(${DINRoundProMedi}) format('woff2')
      `,
  unicodeRange:
    "U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF"
};

const DINRoundProBold_Theme = {
  fontFamily: "DINRoundProBold",
  fontStyle: "normal",
  fontWeight: "bold",
  src: `
        local('DINRoundProBold'),
        url(${DINRoundProBold}) format('woff2')
      `,
  unicodeRange:
    "U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF"
};

const DINRoundProBlack_Theme = {
  fontFamily: "DINRoundProBlack",
  fontStyle: "normal",
  fontWeight: 900,
  src: `
        local('DINRoundProBlack'),
        url(${DINRoundProBlack}) format('woff2')
      `,
  unicodeRange:
    "U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF"
};

const theme = createMuiTheme({
  typography: {
    fontFamily: [
      "DINRoundProBlack",
      "DINRoundProBold",
      "DINRoundProMedi",
      "DINRoundProLight",
      "DINRoundPro"
    ]
  },
  palette: {
    primary: {
      main: "#00a9ff"
    },
    secondary: {
      main: "#1d2361"
    }
  },
  overrides: {
    MuiCssBaseline: {
      "@global": {
        "@font-face": [
          DINRoundPro_Theme,
          DINRoundProMedi_Theme,
          DINRoundProLight_Theme,
          DINRoundProBold_Theme,
          DINRoundProBlack_Theme
        ]
      }
    }
  }
});

export default theme;
