import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import { ThemeProvider, CssBaseline } from "@material-ui/core";
import PageNotFound from "../../components/PageNotFound";
import SignUp from "../auth/signup/index";
import history from "../../store/history";
import theme from "./theme";

const RootContainer = () => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Router history={history}>
        <Switch>
          <Route exact path="/signup" component={SignUp} />

          <Redirect from="/" exact to="/signup" />
          <Route
            exact
            path="*"
            render={(props) => <PageNotFound {...props} />}
          />
        </Switch>
      </Router>
    </ThemeProvider>
  );
};

export default RootContainer;
