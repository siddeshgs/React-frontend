import React from "react";
import { Grid, makeStyles } from "@material-ui/core";
import "./styles.css";

const useStyles = makeStyles((theme) => ({
  signText: {
    height: 71,
    // -webkit-filter: blur(10px);
    // filter: blur(10px);
    fontFamily: "DINRoundPro",
    fontSize: 30,
    fontWeight: "normal",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: 1.29,
    letterSpacing: "normal",
    textAlign: "left",
    color: "#ffffff",
    marginLeft: 40,
    marginTop: 30
  },
  headingText1: {
    // width: 784,
    height: 98,
    // -webkit-filter: blur(10px),
    // filter: blur(10px),
    backgroundColor: " #4d57b0"
  }
}));

const SignUp = () => {
  const classes = useStyles();

  return (
    <div className="margin-top-50">
      <Grid container justify="space-between" alignItems="center">
        <Grid className="blockShow">
          <div className={classes.signText}>Signup</div>
          <div className={classes.headingText1}>
            <div>Your class is</div>
          </div>
          <div className={classes.headingText1}>
            <div>only a click</div>
          </div>
          <div className={classes.headingText1}>
            <div>away..</div>
          </div>
        </Grid>
        {/* <Grid></Grid> */}
      </Grid>
    </div>
  );
};

export default SignUp;
