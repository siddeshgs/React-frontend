import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
// import { authReducer } from "../containers/auth/state/reducers";
// import { dashboardReducer } from "../containers/dashboard/state/reducers";
// import { strategiesReducer } from "../containers/strategies/state/reducers";
// import { backtestReducer } from "../containers/backtest/state/reducers";

export const rootReducer = combineReducers({
  routing: routerReducer
  //   auth: authReducer,
  //   dashboard: dashboardReducer,
  //   strategies: strategiesReducer,
  //   backtest: backtestReducer
});
