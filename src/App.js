import React from "react";
import RootContainer from "./containers/root";

const App = () => {
  return <RootContainer />;
};

export default App;
